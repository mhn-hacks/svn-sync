#!/usr/bin/env bash

function enumerate_remote_path {
	local repo="$1"
	local data_path="$2"
	local path="$3"
	
	path="${path#$data_path}"
	path="${path#$repo}"
	path="${path#/}"
	local rev="${path##*@}"
	path="${path%@*}"
	
	>&2 echo -n "Fetching file hierarchy in '$path'..."
	
	local svn_info="$(svn info "$repo/$path")"
	local node_kind=$(echo "$svn_info" | grep '^Node Kind: ')
	node_kind="${node_kind#Node Kind: }"
	if [ "$rev" = "$path" ]
	then
		local rev=$(echo "$svn_info" | grep '^Last Changed Rev: ')
		rev="${rev#Last Changed Rev: }"
	fi
	
	while read line
	do
		if [ "${line:(-1)}" = "/" ]
		then
			continue
		fi
		
		if [ "$node_kind" = "directory" ]
		then
			#echo "path: $path/$line@$rev"
			PATHS="$PATHS
$path/$line@$rev"
		else
			#echo "path: $path@$rev"
			PATHS="$PATHS
$path@$rev"
		fi
	done < <(svn list -R "$repo/$path")
	
	>&2 echo "done."
}

function enumerate_remote_rev {
	local repo="$1"
	local rev="$2"
	local prev=$((rev-1))
	
	>&2 echo -n "Fetching modified paths in revision '$rev'..."

	while read line
	do
		read state path <<< "$line"
		path="${path#$repo}"
		path="${path#/}"
		#echo "path: $path@$rev"
		PATHS="$PATHS
$path@$rev"
	done < <(svn diff --summarize "-r$prev:$rev" "$repo")
	
	>&2 echo "done."
}

source "${BASH_SOURCE%/*}/svn_sync.sh"

PATHS=""

for string in "$@"
do
	case "$string" in
		'') echo "empty" ;;
		*[!0-9]*) enumerate_remote_path "$REPO" "$DATA_PATH" "$string" ;;
		*)        enumerate_remote_rev  "$REPO" "$string" ;;
	esac
done

add_paths "$DATA_CFG" <<< "$PATHS"
sync_paths "$DATA_PATH" "$DATA_SHARED" "$REPO" <<< "$PATHS"
