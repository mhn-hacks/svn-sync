#!/usr/bin/env bash

## For backward compatibility, convert old style paths to new.
## New format does not handle resize information, so it goes for bread.
function convert_to_new_format_if_necessary {
	local p="$1"
	local word=""
	local rev=""
	local svn_path=""
	local options=""
	
	read word rev svn_path options <<< "$p"
	
	if [ "$word" = "path" ]
	then
		path="$svn_path@$rev"
	fi
}
function convert_to_old_format {
	local str="$1"
	
	local p=""
	local rev=""
	
	read p rev <<< "$str"
	
	if ! [ "$p" = "path" ]
	then
		p=${str%@*}
		rev=${str##*@}
		path="path $rev $p"
	fi
}

## Generates list of modified files
function check_modified_files {
	local md5_file="$1"
	
	>&2 echo -n "Checking for locally modified files..."
	local line=""
	local p=""
	
	while read line
	do
		p=${line%:*}
		MODIFIED_FILES["$p"]=1
	done < <(md5sum --check "$md5_file" 2>/dev/null | grep 'FAILED$')
	>&2 echo "done."
	
	#for p in "${!MODIFIED_FILES[@]}"
	#do
	#	>&2 echo "Locally modified: '$p'"
	#done
}

function mklink {
    if [ "$#" -ge "3" ]; then
        cmd /c mklink "$1" "$(cygpath --windows --absolute "$2")" "$(cygpath --windows --absolute "$3")" | grep -v "symbolic link created"
    else
        cmd /c mklink "$(cygpath --windows --absolute "$1")" "$(cygpath --windows --absolute "$2")" | grep -v "symbolic link created"
    fi
}

function sync_paths {
	local data_path="$1"
	local data_shared="$2"
	local repo="$3"
	
	local path=""
	local barepath=""
	local md5_file="$data_shared/.svn_md5"

	declare -A MODIFIED_FILES
	check_modified_files "$md5_file"
	
	while read path
	do
		if [ -z "$path" ]
		then
			continue
		fi
		
		convert_to_new_format_if_necessary "$path"
		
		barepath=${path%@*}
		
		local local_path="$data_shared/$path"
		
		## Download path if it is missing, or if it has been locally modified
		if ! [ -f "$local_path" ] || [ ${MODIFIED_FILES["$local_path"]+_} ];
		then
			>&2 echo "Fetching $path"
			mkdir -p "$(dirname "$local_path")"
			if svn export --quiet --force "$repo/$path" "$local_path@"
			then
				touch "$md5_file"
				sed -i -n "\# [ \*]$local_path#!p" "$md5_file"
				md5sum "$local_path" >> "$md5_file"
			
			fi
		fi
		
		## For backward compatibility, create path without revision
		if ! [ "$barepath" = "$path" ]
		then
			rm -f "$data_path/$barepath"
			mkdir -p "$(dirname "$data_path/$barepath")"
			
			case "$(uname -s)" in
				CYGWIN*|MINGW32*|MSYS*)
					mklink "$data_path/$barepath" "$local_path" ;;
				*)
					ln -s "$local_path" "$data_path/$barepath" ;;
			esac
		fi
	done #< <(sort -u "$data_cfg")
}

function add_paths {
	local data_cfg="$1"
	
	tmpfile=$(mktemp)
	
	cat - "$data_cfg" > "$tmpfile"
	
	sort -rg -t\@ -k 2,2 "$tmpfile" | sort -u -t\@ -k 1,1 > "$data_cfg"
	
	rm -f "$tmpfile"
}

DATA_CFG="data.cfg"
DATA_PATH="data"
DATA_SHARED="data_shared"
#REPO="svn://big/grafik"
REPO="svn://svn.code.sf.net/p/codeblocks/code/trunk"
#REPO="file:///home/mhn/mhn/Development/svn_sync/repo/test1/trunk"

if [[ "$(basename -- "$0")" == "$(basename -- "${BASH_SOURCE[0]}")" ]]
then
	sync_paths "$DATA_PATH" "$DATA_SHARED" "$REPO" < "$DATA_CFG"
fi