#!/bin/bash


function enumerate_local_path {
	local data_path="$1"
	local path="$2"
	
	local path="$data_path/$path"
	
	>&2 echo -n "Enumerating files in $path..."
	
	local p="$path"
	while ! [ "$(dirname "$p")" = "$data_path" ]
	do
		p=$(dirname "$p")
		local dir="${p#$data_path}"
		dir="${dir#/}"
		DIR_LIST+=("$dir")
	done
	
	while read line
	do
		line="${line#$data_path}"
		line="${line#/}"
		DIR_LIST+=("$line")
	done < <(find "$path" -not -empty -type d)

	while read line
	do
		line="${line#$data_path}"
		line="${line#/}"
		FILE_LIST+=("$line")
	done < <(find -L "$path" -regextype posix-extended -not -regex '.*@[[:digit:]]*$' -type f)
	
	>&2 echo "done."
}

function filter_dirs {
	local repo="$1"
	
	>&2 echo -n "Checking what directories to create on remote repo..."
	
	IFS=$'\n' sorted_dirs=($(sort <<<"${DIR_LIST[*]}"))
	DIR_LIST=()
	
	for path in "${sorted_dirs[@]}"
	do
		>&2 echo -n "."
		if ! svn ls "$repo/$path" --depth empty 2>/dev/null
		then
			DIR_LIST+=("$path")
		fi
	done
	
	>&2 echo "done."
}

function filter_files {
	local data_path="$1"
	local repo="$2"
	local data_cfg="$3"
	
	>&2 echo -n "Checking what files has been locally modified..."
	
	IFS=$'\n' sorted_files=($(sort <<<"${FILE_LIST[*]}"))
	FILE_LIST=()
	
	for path in "${sorted_files[@]}"
	do
		>&2 echo -n "."
		
		local local_path="$data_path/$path"
		local path_and_rev="$(grep -E "^$path(@[0-9]+)?\$" "$data_cfg")" 
		
		if [ -z "$path_and_rev" ]
		then
			path_and_rev="$path"
		fi
		
		local remote_hash=$(svn cat "$repo/$path_and_rev" 2>/dev/null | md5sum | awk '{ print $1 }')
		local local_hash_info="$(md5sum "$local_path")"
		local local_hash="${local_hash_info%% *}"
		
		if ! [ "$local_hash" = "$remote_hash" ]
		then
			FILE_LIST+=("$path")
		fi
		
	done
	
	>&2 echo "done."
}

function create_commit {
	local data_path="$1"
	local repo="$2"
	
	if [ ${#FILE_LIST[@]} -gt 0 ]
	then
		local cmd=(svnmucc --root-url "$repo")
		local msg="

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# Changes to be committed:
#"
	
		for d in "${DIR_LIST[@]}"
		do
			cmd+=(mkdir "$d")
			msg="$msg
#       new directory: $d"
		done
		
		for f in "${FILE_LIST[@]}"
		do
			cmd+=(put "$data_path/$f" "$f")
			msg="$msg
#       modified: $f"
			>&2 echo "file: $f"
		done
		
		tmpfile=$(mktemp)
		
		cmd+=(-F "$tmpfile")

		echo "$msg" > "$tmpfile"

		"${FCEDIT:-${VISUAL:-${EDITOR:-vi}}}" "$tmpfile"

		sed -i -e '/\s*#.*$/d' -e '/^\s*$/d' "$tmpfile"

		if [ -s "$tmpfile" ]
		then
			local resp="$(${cmd[@]})"
			#r2 committed by mhn at 2016-09-29T10:56:05.809712Z
			NEW_REV=$(expr match "$resp" 'r\([[:digit:]]\+\) committed by .* at .*')
			if [ -z "$NEW_REV" ]
			then
				>&2 echo "$resp"
			fi
		else
			echo "Aborting commit due to empty commit message."
		fi
		rm -f "$tmpfile"
	else
		echo "No files to commit, aborting"
	fi
}

source "${BASH_SOURCE%/*}/svn_sync.sh"

DIR_LIST=()
FILE_LIST=()
NEW_REV=""

for path in "$@"
do
	path="${path#$DATA_PATH}"
	path="${path#/}"
	enumerate_local_path "$DATA_PATH" "$path"
done

filter_files "$DATA_PATH" "$REPO" "$DATA_CFG"
filter_dirs "$REPO"

create_commit "$DATA_PATH" "$REPO"

if [ -n "$NEW_REV" ]
then
	PATHS=""
	for f in "${FILE_LIST[@]}"
	do
		PATHS="$PATHS
$f@$NEW_REV"
	done
	add_paths "$DATA_CFG" <<< "$PATHS"
	sync_paths "$DATA_PATH" "$DATA_SHARED" "$REPO" <<< "$PATHS"
fi










# repo="svn://big/grafik/"
# path="$1"

# if [ -d "$path" ]
# then
  # pathpart="$path"
  # files="$path"/*
# else
  # pathpart="$(dirname $path)"
  # files="$path"
# fi

# svnpath="${pathpart#data/content/}"

# dirlist=()
# filelist=()
# dir="."
# SAVEIFS=$IFS
# IFS='/'
# for d in $svnpath
# do
  # dir="$dir/$d"
  # dirlist+=("$dir")
# done
# IFS=$SAVEIFS

# while read line
# do
  # dir="${line#data/content/}"
  # dirlist+=("./$dir")
# done < <(find "$path" -mindepth 1 -type d)

# while read line
# do
  # file="${line#data/content/}"
  # filelist+=("./$file")
# done < <(find "$path" -type f)


# cmd="svnmucc -U $repo"
# shouldcommit=false
# msg="

# # Please enter the commit message for your changes. Lines starting
# # with '#' will be ignored, and an empty message aborts the commit.
# # Changes to be committed:
# #"

# for d in "${dirlist[@]}"
# do
  # >&2 echo -n "$d"
  # if ! svn ls "$repo/$d" --depth empty 2>/dev/null
  # then
# #    echo "mkdir $d"
    # >&2 echo " [New]"
    # cmd="$cmd mkdir $d"
    # msg="$msg
# # new directory: $d"
  # else
    # >&2 echo
  # fi
# done


# for f in "${filelist[@]}"
# do
  # >&2 echo -n "$f"
  # rhash=$(svn cat "$repo/$f" 2>/dev/null | sha1sum | awk '{ print $1 }')
  # lhash=$(sha1sum "data/content/$f" | awk '{ print $1 }')
  # if ! [ "$lhash" = "$rhash" ]
  # then
    # shouldcommit=true
    # >&2 echo " [Updated]"
# #    echo "file: $f $lhash $rhash"
    # cmd="$cmd put data/content/$f $f"
    # msg="$msg
# # updated: $f"
  # else
    # >&2 echo
  # fi
# done

# >&2 echo
# >&2 echo

# if ! command -v svnmucc >/dev/null 2>&1
# then
  # echo "Command 'svnmucc' missing, install package...:"
  # yum whatprovides -C --noplugins svnmucc
# elif [ "$shouldcommit" = true ]
# then
  # tmpfile=$(mktemp)

  # echo "$msg" > "$tmpfile"

  # if [ "x$VISUAL" == x ]
  # then
    # VISUAL="$EDITOR"
  # fi

  # "${VISUAL:-vi}" "$tmpfile"

  # sed -i -e '/\s*#.*$/d' -e '/^\s*$/d' "$tmpfile"

  # if [ -s "$tmpfile" ]
  # then
    # $cmd -F "$tmpfile"
  # else
    # echo "Aborting commit due to empty commit message."
  # fi
  # rm -f "$tmpfile"
# else
  # echo "No files to commit, aborting."
# fi
